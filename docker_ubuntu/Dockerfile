### --------------------------------------------------------------------
### Docker Build Arguments
### Available only during Docker build - `docker build --build-arg ...`
### --------------------------------------------------------------------
ARG APP_BUILD_TYPE="Release"
ARG APP_USER_NAME="appuser"
ARG APP_USER_ID="1000"
ARG APP_GROUP_NAME="appgroup"
ARG APP_GROUP_ID="1000"
ARG APP_MOUNT_VOLUME="false"
### --------------------------------------------------------------------


### --------------------------------------------------------------------
### Base image
### --------------------------------------------------------------------
FROM ubuntu:22.04 as base

# Fix tzdata hang
ENV TZ=Etc/UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN \
    apt-get update && \
    apt-get install -y \
    git zip unzip \
    make cmake g++ \
    libcurl4-openssl-dev libssl-dev libpulse-dev \
    uuid-dev zlib1g-dev


### --------------------------------------------------------------------
### Build aws-sdk-cpp
### --------------------------------------------------------------------
FROM base as build-aws-sdk-cpp

RUN ln -sf /bin/bash /bin/sh
COPY dependencies.sh dependencies.sh
RUN sh dependencies.sh

### --------------------------------------------------------------------
### Build the application
### --------------------------------------------------------------------
FROM build-aws-sdk-cpp as build-app
ARG APP_MOUNT_VOLUME
WORKDIR /code/
COPY . .
ARG APP_BUILD_TYPE
ENV APP_BUILD_TYPE="${APP_BUILD_TYPE}"
RUN rm -rf build && cmake -S . -B build -DCMAKE_BUILD_TYPE="${APP_BUILD_TYPE}"
WORKDIR /code/build/
RUN make && if [[ "$APP_MOUNT_VOLUME" = "true" ]] ; then rm -rf /code ; fi


### --------------------------------------------------------------------
### Final application image
### --------------------------------------------------------------------
FROM ubuntu:22.04 as app
RUN apt-get update && \
    apt-get install -y libcurl4

ARG APP_USER_NAME
ARG APP_USER_ID
ARG APP_GROUP_ID
ARG APP_GROUP_NAME

RUN groupadd --gid "$APP_GROUP_ID" "$APP_GROUP_NAME" \
    && useradd --uid "$APP_USER_ID" --gid "${APP_GROUP_ID}" --shell /bin/bash "$APP_USER_NAME"
USER "$APP_USER_NAME"
COPY --from=build-app /usr/local/lib/*.so* /usr/local/lib/
COPY --from=build-app /usr/local/bin/. /usr/local/bin/
# ENTRYPOINT [ "/usr/local/bin/s3-demo" ]
