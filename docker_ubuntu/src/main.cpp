/*
   Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
   SPDX-License-Identifier: Apache-2.0
*/

#include <iostream>
#include <fstream>
#include <chrono>

#include <sys/stat.h>
#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include "awsdoc/s3/s3_examples.h"

/**
 *
 * main function
 *
 * TODO(user): items: Set the following variables:
 * - bucketName: The name of the bucket.
 * - fileName: The name of the file to add.
 *
*/
#ifndef TESTING_BUILD

int main() {
    Aws::SDKOptions options;
    Aws::InitAPI(options);
    {
        //TODO(user): Create bucket_name to the name of a bucket in your account.
        const Aws::String bucket_name = "s3connectviacpp";
        //TODO(user): Create a file called like one of the files in the local folder where your executables are built to.
        const Aws::String object_get_name = "JoJo's Bizarre Adventure Opening 4.mp4";
        const Aws::String object_put_name = "gothic2Map.jpg";
        Aws::Client::ClientConfiguration clientConfig;
        // Optional: Set to the AWS Region in which the bucket was created (overrides config file).
        clientConfig.region = "eu-central-1";
        //ENTER COMMANDS

        AwsDoc::S3::GetObject(object_put_name, bucket_name, clientConfig);
        
        for (int i{0}; i < 100; i++) {
            {
                AwsDoc::S3::PutObject(object_put_name, bucket_name, clientConfig);
            }
            {
                AwsDoc::S3::DeleteObject(object_put_name, bucket_name, clientConfig);
            }
            {
                AwsDoc::S3::GetObject(object_get_name, bucket_name, clientConfig, "C:\\");
            }
        }

        AwsDoc::S3::PutObject(bucket_name, object_get_name, clientConfig);
        
        
    }

    Aws::ShutdownAPI(options);

    return 0;
}


#endif  // TESTING_BUILD
