#include "s3_examples.h"


#include <iostream>
#include <fstream>
#include <filesystem>
#include <sys/stat.h>


#include <aws/s3/model/PutObjectRequest.h>
#include <aws/s3/model/GetObjectRequest.h>
#include <aws/s3/model/DeleteObjectRequest.h>

bool AwsDoc::S3::PutObject(const Aws::String& fileName, 
    const Aws::String& bucketName,
    const Aws::Client::ClientConfiguration& clientConfig,
    const Aws::String& sysPath) {

    Aws::S3::S3Client s3_client(clientConfig);

    Aws::S3::Model::PutObjectRequest request;
    request.SetBucket(bucketName);
    //We are using the name of the file as the key for the object in the bucket.
    //However, this is just a string and can be set according to your retrieval needs.
    request.SetKey(fileName);

    if (!std::filesystem::is_directory(sysPath)) {
        std::cerr << "Error: Path " << sysPath << " is not found " << std::endl;
        return false;
    }

    std::shared_ptr<Aws::IOStream> inputData;

    if (sysPath.back() == '/' || sysPath.back() == '\\') {
        inputData =
            Aws::MakeShared<Aws::FStream>("SampleAllocationTag",
                sysPath + fileName.c_str(),
                std::ios_base::in | std::ios_base::binary);
    }
    else {
        inputData =
            Aws::MakeShared<Aws::FStream>("SampleAllocationTag",
                sysPath + "/" + fileName.c_str(),
                std::ios_base::in | std::ios_base::binary);
    };

    if (!*inputData) {
        std::cerr << "Error unable to read file " << fileName << std::endl;
        return false;
    }

    request.SetBody(inputData);


    Aws::S3::Model::PutObjectOutcome outcome =
        s3_client.PutObject(request);

    if (!outcome.IsSuccess()) {
        std::cerr << "Error: PutObject: " <<
            outcome.GetError().GetMessage() << std::endl;
    }
    else {
        std::cout << "Added object '" << fileName << "' to bucket '"
            << bucketName << "'." << std::endl;
    }

    return outcome.IsSuccess();
}

bool AwsDoc::S3::GetObject(const Aws::String& objectKey,
    const Aws::String& fromBucket,
    const Aws::Client::ClientConfiguration& clientConfig,
    const Aws::String& savePath) {
    Aws::S3::S3Client client(clientConfig);

    Aws::S3::Model::GetObjectRequest request;
    request.SetBucket(fromBucket);
    request.SetKey(objectKey);

    Aws::S3::Model::GetObjectOutcome outcome =
        client.GetObject(request);

    if (!outcome.IsSuccess()) {
        const Aws::S3::S3Error& err = outcome.GetError();
        std::cerr << "Error: GetObject: " <<
            err.GetExceptionName() << ": " << err.GetMessage() << std::endl;
    }
    else {
        if (!std::filesystem::is_directory(savePath)) {
            std::cerr << "Error: Path " << savePath << " is not found " << std::endl;
            return false;
        }
        // Get the response data
        auto& object = outcome.GetResult();
        auto& stream = object.GetBody();


        std::string filePath;
        if (savePath.back() == '/' || savePath.back() == '\\') {
            filePath = savePath + objectKey;
        }
        else {
            filePath = savePath + "/" + objectKey;
        }

        // Create a local file to save the downloaded object
        std::ofstream outputFile(filePath, std::ios::binary);

        // Read the object data from the stream and write it to the local file
        outputFile << stream.rdbuf();

        std::cout << "Successfully retrieved '" << objectKey << "' from '"
            << fromBucket << "'." << std::endl;
    }

    return outcome.IsSuccess();
}

bool AwsDoc::S3::DeleteObject(const Aws::String& objectKey,
    const Aws::String& fromBucket,
    const Aws::Client::ClientConfiguration& clientConfig) {
    Aws::S3::S3Client client(clientConfig);
    Aws::S3::Model::DeleteObjectRequest request;

    request.WithKey(objectKey)
        .WithBucket(fromBucket);

    Aws::S3::Model::DeleteObjectOutcome outcome =
        client.DeleteObject(request);

    if (!outcome.IsSuccess()) {
        auto err = outcome.GetError();
        std::cerr << "Error: DeleteObject: " <<
            err.GetExceptionName() << ": " << err.GetMessage() << std::endl;
    }
    else {
        std::cout << "Successfully deleted the object." << std::endl;
    }

    return outcome.IsSuccess();
}

