// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX - License - Identifier: Apache - 2.0 

#pragma once



#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/BucketLocationConstraint.h>

namespace AwsDoc {
    namespace S3 {
        bool PutObject(const Aws::String& fileName, 
            const Aws::String& bucketName,
            const Aws::Client::ClientConfiguration& clientConfig,
            const Aws::String& sysPath = ".\\");

        bool GetObject(const Aws::String& objectKey,
            const Aws::String& fromBucket,
            const Aws::Client::ClientConfiguration& clientConfig,
            const Aws::String& savePath = ".\\");

        bool DeleteObject(const Aws::String& objectKey,
            const Aws::String& fromBucket,
            const Aws::Client::ClientConfiguration& clientConfig);

        extern std::mutex upload_mutex;

        extern std::condition_variable upload_variable;
    } // namespace S3
} // namespace AwsDoc