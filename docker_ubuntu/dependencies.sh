apt-get update && apt-get install -y libasio-dev
mkdir crow
cd crow
git clone https://github.com/CrowCpp/Crow.git
cmake ./Crow -DCROW_BUILD_EXAMPLES=OFF -DCROW_BUILD_TESTS=OFF
make install
cd ..
mkdir aws_sdk
cd aws_sdk
git clone --recurse-submodules --single-branch --branch 1.11.111 https://github.com/aws/aws-sdk-cpp
cmake ./aws-sdk-cpp -DBUILD_ONLY="s3" -DCMAKE_BUILD_TYPE="Release" -DENABLE_TESTING=OFF
make
make install
